import falcon
import json

class MainResource(object):
    def on_get(self, req, resp, **params):
        env = str(req.env)
        body = env.replace("{'", "{<br/>&nbsp;&nbsp;'").replace("'}", "'<br/>}").replace(", '", ",<br/>&nbsp;&nbsp;'")
        print(env)
        resp.body = body
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/html'

        return True

