import os
import falcon
from resources import MainResource

api = falcon.API()
api.add_route('/', MainResource())
